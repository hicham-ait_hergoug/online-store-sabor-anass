<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\Admin\AdminSuperController;
use App\Http\Controllers\Admin\AdminProductController;
use App\Http\Controllers\Admin\AdminDiscountController;
use App\Http\Controllers\Admin\AdminSupplierController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/admin/create', [AdminController::class,'create'])->name('admin.create');

Route::get('/', 'App\Http\Controllers\HomeController@index')->name("home.index");
Route::get('/about', 'App\Http\Controllers\HomeController@about')->name("home.about");
Route::get('/products', 'App\Http\Controllers\ProductController@index')->name("product.index");
Route::get('/products/{id}', 'App\Http\Controllers\ProductController@show')->name("product.show");
// Route::get('/products/filtrer', 'App\Http\Controllers\ProductController@viewByCategory')->name("product.viewByCategory");

Route::get('/cart', 'App\Http\Controllers\CartController@index')->name("cart.index");
Route::get('/cart/delete', 'App\Http\Controllers\CartController@delete')->name("cart.delete");
Route::delete('/cart/delete', [CartController::class, 'delete'])->name('cart.delete');

Route::post('/cart/add/{id}', 'App\Http\Controllers\CartController@add')->name("cart.add");

Route::middleware('auth')->group(function () {
    Route::get('/cart/purchase', 'App\Http\Controllers\CartController@purchase')->name("cart.purchase");
    Route::get('/my-account/orders', 'App\Http\Controllers\MyAccountController@orders')->name("myaccount.orders");
});
// Route::middleware(['admin','super_admin'])->group(function () {
    Route::group(['middleware' => ['auth','role:admin|superadmin']], function () {
        Route::get('/admin', 'App\Http\Controllers\Admin\AdminHomeController@index')->name("admin.home.index");
        Route::get('/admin/products', 'App\Http\Controllers\Admin\AdminProductController@index')->name("admin.product.index");
        Route::get('/admin/products/create', [AdminProductController::class, 'create'])->name('admin.products.create');

        Route::post('/admin/products/store', 'App\Http\Controllers\Admin\AdminProductController@store')->name("admin.product.store");
        Route::delete('/admin/products/{id}/delete', 'App\Http\Controllers\Admin\AdminProductController@delete')->name("admin.product.delete");
        Route::get('/admin/products/{id}/edit', 'App\Http\Controllers\Admin\AdminProductController@edit')->name("admin.product.edit");
        Route::put('/admin/products/{id}/update', 'App\Http\Controllers\Admin\AdminProductController@update')->name("admin.product.update");
 //categorie
        Route::get('/admin/categories', 'App\Http\Controllers\Admin\AdminCategoryController@index')->name("admin.category.index");
// Route::get('/admin/categories/create', 'App\Http\Controllers\Admin\AdminCategoryController@create')->name("admin.category.create");
        Route::get('/admin/categories/create', 'App\Http\Controllers\Admin\AdminCategoryController@create')->name("admin.category.create");
        Route::post('/admin/categories/store', 'App\Http\Controllers\Admin\AdminCategoryController@store')->name("admin.category.store");
        Route::delete('/admin/categories/{id}/delete', 'App\Http\Controllers\Admin\AdminCategoryController@destroy')->name("admin.category.destroy");
        Route::get('/admin/categories/{id}/edit', 'App\Http\Controllers\Admin\AdminCategoryController@edit')->name("admin.category.edit");
        Route::put('/admin/categories/{id}/update', 'App\Http\Controllers\Admin\AdminCategoryController@update')->name("admin.category.update");
 //discount
        Route::get('/admin/discounts', [AdminDiscountController::class, 'index'])->name('admin.discounts.index');
        Route::get('/admin/discounts/create', [AdminDiscountController::class, 'create'])->name('admin.discounts.create');
        Route::post('/admin/discounts', [AdminDiscountController::class, 'store'])->name('admin.discounts.store');
        Route::get('/admin/discounts/{discount}', [AdminDiscountController::class, 'show'])->name('admin.discounts.show');
        Route::get('/admin/discounts/{discount}/edit', [AdminDiscountController::class, 'edit'])->name('admin.discounts.edit');
        Route::put('/admin/discounts/{discount}', [AdminDiscountController::class, 'update'])->name('admin.discounts.update');
        Route::delete('/admin/discounts/{discount}', [AdminDiscountController::class, 'destroy'])->name('admin.discounts.destroy');
 //supplier
        Route::get('/admin/suppliers', [AdminSupplierController::class, 'index'])->name('admin.suppliers.index');
        Route::get('/admin/suppliers/create', [AdminSupplierController::class, 'create'])->name('admin.suppliers.create');
        Route::post('/admin/suppliers', [AdminSupplierController::class, 'store'])->name('admin.suppliers.store');
        Route::get('/admin/suppliers/{supplier}', [AdminSupplierController::class, 'show'])->name('admin.suppliers.show');
        Route::get('/admin/suppliers/{supplier}/edit', [AdminSupplierController::class, 'edit'])->name('admin.suppliers.edit');
        Route::put('/admin/suppliers/{supplier}', [AdminSupplierController::class, 'update'])->name('admin.suppliers.update');
        Route::delete('/admin/suppliers/{supplier}', [AdminSupplierController::class, 'destroy'])->name('admin.suppliers.destroy');
//super admin
// Route::get('/admin/super-admins', [SuperAdminController::class, 'index'])->name('admin.superAdmin.index');
    });
    Auth::routes();
    Route::middleware('super_admin')->group(function () {
        Route::resource('/admin/superAdmin', AdminSuperController::class);
    });

    Auth::routes();
