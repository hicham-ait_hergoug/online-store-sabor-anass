@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $viewData['subtitle'] }}</h1>
        @if (count($viewData['products']) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td>{{ $product->getName() }}</td>
                            <td>
                                @if ($product->hasActiveDiscount())
                                <strong> {{ $product->getSalePrice() ? $product->getSalePrice() : $product->getPrice() }}€</strong>
                                @else
                                    <strong>{{ $product->getPrice() }}€</strong>
                                @endif
                            <td><strong>{{ $productsInCookie[$product->getId()] }}</strong></td>
                            {{-- <td>{{ $productsInCookie[$product->getId()] * ($product->getSalePrice() ? $product->getSalePrice() : $product->getPrice()) }} --}}
                                <td><strong>{{ $productsInCookie[$product->getId()] * ($product->hasActiveDiscount() ? $product->getSalePrice() : $product->getPrice()) }}€</strong></td>

                            </td>

                            <td>
                                <form action="{{ route('cart.delete') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="product_id" value="{{ $product->getId() }}">
                                    <button type="submit" class="btn btn-danger">Remove</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" class="text-right"><strong>Total:</strong></td>
                        <td><strong>{{ $viewData['total'] }} €</strong></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-right">
                <a href="{{ route('cart.delete') }}" class="btn btn-danger">Empty Cart</a>
                <a href="{{ route('product.index') }}" class="btn btn-primary">Continue Shopping</a>
                @if (Auth::check() && Auth::user()->getBalance() >= $viewData['total'])
                    <a href="{{ route('cart.purchase') }}" class="btn btn-success">purchase</a>
                @endif
            </div>
        @else
            <p>Your cart is empty.</p>
        @endif
    </div>
@endsection
