@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>

    <div class="card">
        <div class="card-header bg-dark text-white text-center text-capitalize">
            <h1>Admin Panel - Home Page</h1>
        </div>

        <div class="card-body">
            <div class="row">

                <div class="col-md-6 mb-3">
                    <canvas id="product-chart" style="width:100%;max-width:600px"></canvas>
                </div>

                <div class="col-md-6 mb-3">
                    <canvas id="stock-chart" style="width:100%;max-width:600px"></canvas>
                </div>
                <div class="card">
                    <div>
                        <canvas id="chiffreAffaireParJourChart" style="width:100%;max-width:600px"></canvas>
                    </div>
                </div>

            </div>
        </div>
        @php
            $productsByCategory = DB::table('products')
                ->select('categories.intitule as category', DB::raw('count(products.id) as count'))
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->groupBy('categories.intitule')
                ->orderBy('count', 'ASC')
                ->get();
            
            function getRandomColor()
            {
                $letters = '0123456789ABCDEF';
                $color = '#';
                for ($i = 0; $i < 6; $i++) {
                    $color .= $letters[rand(0, 15)];
                }
                return $color;
            }
            $backgroundColor = [];
            for ($i = 0; $i < count($productsByCategory); $i++) {
                $backgroundColor[] = getRandomColor();
            }
        @endphp


        <script>
            var productsByCategory = @json($productsByCategory);
            var backgroundColor = @json($backgroundColor);

            var ctx = document.getElementById('product-chart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: productsByCategory.map(function(product) {
                        return product.category;
                    }),
                    datasets: [{
                        label: 'Number of products',
                        data: productsByCategory.map(function(product) {
                            return product.count;
                        }),
                        backgroundColor: backgroundColor,
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: true,
                        text: 'Number of Products per Category'
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        </script>

        @php
            $products = DB::table('products')
                ->select('name', 'quantity_store')
                ->orderBy('quantity_store', 'DESC')
                ->get();
        @endphp
        <script>
            var products = @json($products);

            var ctx = document.getElementById('stock-chart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: products.map(function(product) {
                        return product.name;
                    }),
                    datasets: [{
                        label: 'Quantity in stock',
                        data: products.map(function(product) {
                            return product.quantity_store;
                        }),
                        backgroundColor: products.map(function(product) {
                            if (product.quantity_store == 0) {
                                return 'rgba(255, 0, 0, 0.2)'; // rouge pour quantité de stock = 0
                            } else if (product.quantity_store < 10) {
                                return 'rgba(255, 165, 0, 0.2)'; // orange pour quantité de stock < 10
                            } else {
                                return 'rgba(0, 255, 0, 0.2)'; // vert pour quantité de stock > 10
                            }
                        }),
                        borderColor: products.map(function(product) {
                            if (product.quantity_store == 0) {
                                return 'rgba(255, 0, 0, 1)';
                            } else if (product.quantity_store < 10) {
                                return 'rgba(255, 165, 0, 1)';
                            } else {
                                return 'rgba(0, 255, 0, 1)';
                            }
                        }),
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: true,
                        text: 'Quantity of Products in Stock'
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        </script>

        @php
            // Récupérer les ventes par jour à partir de la base de données
            $ventesParJour = DB::table('orders')
                ->select(DB::raw('date(created_at) as date'), DB::raw('sum(total) as chiffre_affaire'))
                ->groupBy('date')
                ->get();
            
            // Préparer les données pour le graphique
            $labels = $ventesParJour->pluck('date');
            $data = $ventesParJour->pluck('chiffre_affaire');
        @endphp
        <script>
            var ctx = document.getElementById('chiffreAffaireParJourChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: {!! json_encode($labels) !!},
                    datasets: [{
                        label: "Chiffre d'affaire",
                        data: {!! json_encode($data) !!},
                        backgroundColor: "rgba(38, 185, 154, 0.31)",
                        borderColor: "rgba(38, 185, 154, 0.7)",
                        pointBorderColor: "rgba(38, 185, 154, 0.7)",
                        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(220,220,220,1)"
                    }]
                },
                options: {}
            });
        </script>
       <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <div class="card bg-indigo-400">
              <div class="card-body">
                <h5 class="card-title ">Total Visitors</h5>
                <p class="card-text">0</p>
                <a href="{{ url('/visitors') }}" class="btn btn-primary">Details</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Products</h5>
                <p class="card-text">{{ $viewData['products']->count() }}</p>
                <a href="{{ url('/products') }}" class="btn btn-primary">Details</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Orders</h5>
                <p class="card-text">{{ $viewData['orders']->count() }}</p>
                <a href="{{ url('/orders') }}" class="btn btn-primary">Details</a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Users</h5>
                <p class="card-text">{{ $viewData['users']->count() }}</p>
                <a href="{{ url('/users') }}" class="btn btn-primary">Details</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      
@endsection
