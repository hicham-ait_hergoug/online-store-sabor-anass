@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card">
    <div class="card-header bg-dark text-white text-center text-capitalize">
        <h1 class="card-title">{{$viewData['title']}}</h1>
    </div>
    </div>

        @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            @endif
    
    <div>
        <form action="{{ route('admin.discounts.store') }}" method="POST">
            @csrf
        
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" required>
            </div>
        
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" id="description" class="form-control"></textarea>
            </div>
        
            <div class="form-group">
                <label for="value">Discount Value:</label>
                <input type="number" name="value" id="value" class="form-control" required>
            </div>
        
            <div class="form-group">
                <label for="start_date">Start Date:</label>
                <input type="date" name="start_date" id="start_date" class="form-control" required>
            </div>
        
            <div class="form-group">
                <label for="end_date">End Date:</label>
                <input type="date" name="end_date" id="end_date" class="form-control" required>
            </div>
        
            <div class="form-group">
                <label for="apply_to">Apply to:</label>
                <select name="apply_to" id="apply_to" class="form-control" required>
                    <option value="all" >All products</option>
                    <option value="categories">Categories</option>
                    <option value="products">Specific products</option>
                </select>
            </div>
            
        
            <div class="form-group" id="categories" style="display:none">
                <label for="category">Categories:</label>
                <select  name="categories[]" id="category" class="form-control" multiple>
                    @foreach($viewData['categories'] as $category)
                        <option value="{{ $category->getId() }}">{{ $category->getIntitule() }}</option>
                    @endforeach
                </select>
            </div>
        
            <div class="form-group" id="products" style="display:none">
                <label for="product">Products:</label>
                <select name="products[]" id="product" class="form-control" multiple>
                    @foreach($viewData["products"]  as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
        
            <button type="submit" class="btn btn-primary">Create Discount</button>
        </form>
        
        <script>
            // Show or hide the categories or products select box depending on the "apply_to" value
            document.getElementById('apply_to').addEventListener('change', function() {
    if (this.value == 'categories') {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'block';
        // console.log(this.value )
        document.getElementById('products').style.display = 'none';
    } else if (this.value == 'products') {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'none';
        document.getElementById('products').style.display = 'block';
        // console.log(this.value )
    } else {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'none';
        document.getElementById('products').style.display = 'none';
        console.log(this.value )
    }})
        </script>
</div>
@endsection
