@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
    
    <div class="card">
        <div class="card-header bg-dark text-white text-center text-capitalize">
            <h1 class="card-title">{{$viewData['title']}}</h1>
        </div>
        </div>
    
        @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            @endif
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$viewData['title']}}</h3>
                        <div class="card-tools">
                            <a href="{{ route('admin.discounts.create') }}" class="btn btn-primary">
                                <i class="fas fa-plus"></i> Add Discount
                            </a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Value</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Apply To</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($viewData["discounts"] as $discount)
                                    <tr>
                                        <td>{{ $discount->id }}</td>
                                        <td>{{ $discount->name }}</td>
                                        <td>{{ $discount->description }}</td>
                                        <td>{{ $discount->value }}</td>
                                        <td>{{ $discount->start_date }}</td>
                                        <td>{{ $discount->end_date }}</td>
                                        <td>{{ $discount->apply_to }}</td>
                                        <td>
                                            <form action="{{ route('admin.discounts.destroy', $discount) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <a class="btn btn-info btn-sm" href="{{ route('admin.discounts.edit', $discount) }}">
                                                    <i class="fas fa-pencil-alt"></i> Edit
                                                </a>
                                                <button class="btn btn-danger btn-sm" type="submit">
                                                    <i class="fas fa-trash"></i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $viewData["discounts"]->links() }}
                    </div>
                </div>
            @endsection
            
