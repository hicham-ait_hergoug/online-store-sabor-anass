@extends('layouts.admin')

@section('title', 'Edit Discount')

@section('content')
<div class="card">
    <div class="card-header bg-dark text-white text-center text-capitalize">
        <h1 class="card-title">Edit discount</h1>
    </div>
    </div>

    
    @if ($errors->any())
        <ul class="alert alert-danger list-unstyled">
            @foreach ($errors->all() as $error)
                <li>- {{ $error }}</li>
            @endforeach
        </ul>
    @endif
    
    <form action="{{ route('admin.discounts.update', $discount->getId()) }}" method="POST">
        @csrf
        @method('PUT')
        
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $discount->getName() }}" required>
        </div>
        
        <div class="form-group">
            <label for="description">Description:</label>
            <textarea name="description" id="description" class="form-control">{{ $discount->getDescription() }}</textarea>
        </div>
        
        <div class="form-group">
            <label for="value">Discount Value:</label>
            <input type="number" name="value" id="value" class="form-control" value="{{ $discount->getValue() }}" required>
        </div>
        
        <div class="form-group">
            <label for="start_date">Start Date:</label>
            <input type="date" name="start_date" id="start_date" class="form-control" value="{{ old('start_date', optional($discount->start_date)->format('Y-m-d')) }}" required>
        </div>
                
        <div class="form-group">
            <label for="end_date">End Date:</label>
            <input type="date" name="end_date" id="end_date" class="form-control" value="{{ old('end_date', optional($discount->end_date)->format('Y-m-d')) }}" required>
        </div>
        
        
        <div class="form-group">
            <label for="apply_to">Apply to:</label>
            <select name="apply_to" id="apply_to" class="form-control" required>
                <option value="all" @if($discount->getApplyTo() == 'all') selected @endif>All products</option>
                <option value="categories" @if($discount->getApplyTo() == 'categories') selected @endif>Categories</option>
                <option value="products" @if($discount->getApplyTo() == 'products') selected @endif>Specific products</option>
            </select>
        </div>
            
        <div class="form-group" id="categories" style="@if($discount->getApplyTo() != 'categories') display:none @endif">
            <label for="category">Categories:</label>
            <select  name="categories[]" id="category" class="form-control" multiple>
                @foreach($viewData["categories"] as $category)
                    <option value="{{ $category->getId() }}" @if(in_array($category->getId(),$discount->categories()->pluck('category_id')->toArray())) selected @endif>{{ $category->getIntitule()}}</option> --}}
                   
                    {{-- <option value=" {{$category->getId()}}">{{ $category->getIntitule() }}</option> --}}

                @endforeach
            </select>
        </div>
        
        <div class="form-group" id="products" @if ($discount->apply_to !== 'products') style="display:none" @endif>
            <label for="product">Products:</label>
            <select name="products[]" id="product" class="form-control" multiple>
                @foreach($viewData["products"]  as $product)
                    <option value="{{ $product->id }}" @if(in_array($product->id, $discount->products->pluck('id')->toArray())) selected @endif>{{ $product->name }}</option>
                @endforeach
            </select>
        </div>
    
        <button type="submit" class="btn btn-primary">Update Discount</button>
    </form>
    
    <script>
        // Show or hide the categories or products select box depending on the "apply_to" value
        document.addEventListener("DOMContentLoaded", function() {
            var applyToSelect = document.getElementById('apply_to');
            applyToSelect.addEventListener('change', function() {
                var categoriesDiv = document.getElementById('categories');
                var productsDiv = document.getElementById('products');
                
                if (this.value == 'categories') {
                    categoriesDiv.style.display = 'block';
                    productsDiv.style.display = 'none';
                } else if (this.value == 'products') {
                    categoriesDiv.style.display = 'none';
                    productsDiv.style.display = 'block';
                } else {
                    categoriesDiv.style.display = 'none';
                    productsDiv.style.display = 'none';
                }
            });
            
            // Set the initial display of the categories/products select boxes based on the initial "apply_to" value
            var initialApplyToValue = applyToSelect.value;
            if (initialApplyToValue == 'categories') {
                document.getElementById('categories').style.display = 'block';
            } else if (initialApplyToValue == 'products') {
                document.getElementById('products').style.display = 'block';
            }
        });
    </script>
@endsection    