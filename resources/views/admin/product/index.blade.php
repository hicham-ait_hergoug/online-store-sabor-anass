@extends('layouts.admin')

@section('title', $viewData['title'])

@section('content')
    <div class="card">
        <div class="card-header bg-dark text-white text-center text-capitalize">
            <h2>Manage Products</h2>
        </div>
    </div>

    <div class="row mt-3 mb-3">
        <div class="col">
            <div class="input-group">
                <label class="input-group-text" for="category_selected">Filter by Category:</label>
                <select class="form-select" name="category" id="category_selected">
                    <option value="-1">All Categories</option>
                    @foreach ($viewData['categories'] as $item)
                        <option value="{{ $item->getId() }}"
                            {{ $item->getId() == $viewData['category'] ? 'selected' : '' }}>
                            {{ $item->getIntitule() }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="input-group">
                <label class="input-group-text" for="supplier_selected">Filter by Supplier:</label>
                <select class="form-select" name="supplier" id="supplier_selected">
                    <option value="-1">All Suppliers</option>
                    @foreach ($viewData['suppliers'] as $supplier)
                        <option value="{{ $supplier->getId() }}"
                            {{ $supplier->getId() == $viewData['supplier'] ? 'selected' : '' }}>
                            {{ $supplier->getFullName() }}
                        </option>
                    @endforeach
                </select>
            </div>
           
            @if (count($viewData['products']) == 0)
                <h3>No products for this supplier</h3>
            @endif
        </div>
    </div>

    <div class="card-body">
        <table id="myTable"class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Category Name</th>
                    <th scope="col">Quantity in Stock</th>
                    <th scope="col">Supplier Name</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($viewData['products'] as $product)
                    <tr
                        class="@if ($product->getQuantityStore() == 0) bg-danger @elseif($product->getQuantityStore() < 10) bg-warning @else bg-success @endif">
                        <td>{{ $product->getId() }}</td>
                        <td>{{ $product->getName() }}</td>
                        @if ($product->category)
                            <td>{{ $product->category->getIntitule() }}</td>
                        @else
                            <td>No category</td>
                        @endif
                        <td>{{ $product->getQuantityStore() }}</td>
                        @if ($product->supplier)
                            <td>{{ $product->supplier->getFullName() }}</td>
                        @else
                            <td>No supplier</td>
                        @endif
                        <td>
                            <a class="btn btn-primary"
                                href="{{ route('admin.product.edit', ['id' => $product->getId()]) }}">
                                <i class="bi-pencil"></i>
                            </a>
                        </td>
                        <td>
                            <form action="{{ route('admin.product.delete', $product->getId()) }}" method="POST">
                                @csrf
                                @method('DELETE')<div class="modal fade" id="deleteProductModal-{{ $product->getId() }}"
                                    tabindex="-1" aria-labelledby="deleteProductModal-{{ $product->getId() }}-label"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="deleteProductModal-{{ $product->getId() }}-label">Confirm Delete
                                                    Product</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to delete this product?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="btn btn-danger" data-bs-toggle="modal"
                                    data-bs-target="#deleteProductModal-{{ $product->getId() }}">
                                    <i class="bi-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="pagination justify-content-center">
            {{-- {{ $viewData['products']->links() }} --}}
            {{-- {{ $viewData['suppliers']->links() }}
            {{ $viewData['categories']->links() }} --}}
        </div>
    </div>
    <script>
        document.getElementById('category_selected').addEventListener('change', function(e) {
            const cat = this.value;
            window.location.href = '/admin/products?category=' + cat;
            console.log(window.location.href);
        })
        document.getElementById('supplier_selected').addEventListener('change', function(e) {
            const sup = this.value;
            window.location.href = '/admin/products?supplier=' + sup;
        })
    </script>
    
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables.net-buttons/js/buttons.print.min.js" type="77d8de2d019d11b2a57babb2-text/javascript"></script>

<script>
    $(document).ready(function() {
        var table = $('#myTable').DataTable();
    });
</script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
@endsection
