@extends('layouts.admin')
@section('title', 'Suppliers')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-white text-center text-capitalize">
                    <h1 class="card-title">{{$viewData['title']}}</h1>
                </div>
                </div>
            
                    <div class="float-right">
                        <a href="{{ route('admin.category.create') }}" class="btn btn-primary">Add Category</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>id</th>
                                <th>intitule</th>
                                <th>description</th>
                                <th>edit</th>
                                <th>delete</th>
                            </thead>
                            <tbody>
                                @foreach ($viewData['categories'] as $category)
                                    <tr>
                                        <td>{{ $category->getId() }}</td>
                                        <td>{{ $category->getIntitule() }}</td>
                                        <td>{{ $category->getDescription() }}</td>
                                        <td>
                                            <a href="{{ route('admin.category.edit', $category->getId()) }}"><button
                                                    type="submit" class="btn btn-primary">Edit</button></a>
                                        </td>
                                        <td>
                                            <form action="{{ route('admin.category.destroy', $category->getId()) }}"
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center">
                            {{ $viewData["categories"]->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
