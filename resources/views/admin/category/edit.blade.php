@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
<div class="card">
    <div class="card-header bg-dark text-white text-center text-capitalize">
        <h1 class="card-title">{{$viewData['title']}}</h1>
    </div>
    </div>

    @if ($errors->any())
        <ul class="alert alert-danger list-unstyled">
            @foreach ($errors->all() as $error)
                <li>- {{ $error }}</li>
            @endforeach
        </ul>
    @endif



    <form action="{{ route('admin.category.update', $viewData['categories']->getId()) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="fname">Intitule:</label><br>
        <input type="text" id="fname" value="{{ $viewData['categories']->getIntitule() }}" name="intitule"><br>
        <label for="lname">Description:</label><br>
        <textarea name="description" id="lname" cols="30" rows="10">{{ $viewData['categories']->getDescription() }}</textarea> <br><br>
        <input type="submit"class="btn btn-success" value="Submit">
    </form>
@endsection
