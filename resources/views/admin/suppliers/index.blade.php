@extends('layouts.admin')
@section('title', 'Suppliers')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-white text-center text-capitalize">
                    <h1 class="card-title">{{ $viewData['title'] }}</h1>
                </div>
            </div>
            <div class="float-right">
                <a href="{{ route('admin.suppliers.create') }}" class="btn btn-primary">Add Supplier</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="myTable"class="table">
                    <thead class=" text-primary">
                        <th>Full Name</th>
                        <th>Raison sociale</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($viewData['suppliers'] as $supplier)
                            <tr>
                                <td>{{ $supplier->full_name }}</td>
                                <td>{{ $supplier->raison_sociale }}</td>
                                <td>{{ $supplier->email }}</td>
                                <td>{{ $supplier->telephone }}</td>
                                <td>{{ $supplier->city }}</td>
                                <td>{{ $supplier->adresse }}</td>
                                <td>
                                    <a href="{{ route('admin.suppliers.edit', $supplier->id) }}"
                                        class="btn btn-sm btn-primary">Edit</a>
                                    <form action="{{ route('admin.suppliers.destroy', $supplier) }}" method="POST"
                                        style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="pagination justify-content-center">

        {{ $viewData['suppliers']->render() }}
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables.net-buttons/js/buttons.print.min.js" type="77d8de2d019d11b2a57babb2-text/javascript"></script>

    <script>
        $(document).ready(function() {
            var table = $('#myTable').DataTable();
        });
    </script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">

@endsection
