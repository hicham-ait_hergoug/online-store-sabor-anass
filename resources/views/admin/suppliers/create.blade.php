@extends('layouts.admin')

@section('title', 'Create Supplier')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-white text-center text-capitalize">
                    <h1 class="card-title">{{ $viewData['title'] }}</h1>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.suppliers.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="full_name">Full Name:</label>
                        <input type="text" class="form-control" id="full_name" name="full_name"
                            value="{{ old('full_name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="raison_sociale">Raison Sociale:</label>
                        <input type="text" class="form-control" id="raison_sociale" name="raison_sociale"
                            value="{{ old('raison_sociale') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City:</label>
                        <input type="text" class="form-control" id="city" name="city"
                            value="{{ old('city') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="adresse">Adresse:</label>
                        <input type="text" class="form-control" id="adresse" name="adresse"
                            value="{{ old('adresse') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="telephone">Telephone:</label>
                        <input type="text" class="form-control" id="telephone" name="telephone"
                            value="{{ old('telephone') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea class="form-control" id="description" name="description" rows="4">{{ old('description') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a href="{{ route('admin.suppliers.index') }}" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
