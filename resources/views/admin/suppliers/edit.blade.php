@extends('layouts.admin')

@section('title', 'Create Supplier')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-white text-center text-capitalize">
                    <h1 class="card-title">{{ $viewData['title'] }}</h1>
                </div>
            </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.suppliers.update', $viewData["supplier"]->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="full_name">Full Name:</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" value="{{$viewData["supplier"]->getFullName()}}" required>
                        </div>
                        <div class="form-group">
                            <label for="raison_sociale">Raison Sociale:</label>
                            <input type="text" class="form-control" id="raison_sociale" name="raison_sociale" value="{{$viewData["supplier"]->getRaisonSociale()  }}" required>
                        </div>
                        <div class="form-group">
                            <label for="city">City:</label>
                            <input type="text" class="form-control" id="city" name="city"
                                value="{{$viewData["supplier"]->getCity()}}" required>
                        </div>
                        <div class="form-group">
                            <label for="adresse">Adresse:</label>
                            <input type="text" class="form-control" id="adresse" name="adresse" value="{{ $viewData["supplier"]->getAdresse() }}" required>
                        </div>
                        <div class="form-group">
                            <label for="telephone">Telephone:</label>
                            <input type="text" class="form-control" id="telephone" name="telephone" value="{{ $viewData["supplier"]->getTelephone() }}" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $viewData["supplier"]->getEmail()  }}" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" id="description" name="description" rows="4">{{ $viewData["supplier"]->getDescription()  }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{ route('admin.suppliers.index') }}" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
