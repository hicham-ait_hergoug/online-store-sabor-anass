@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')

    <div class="card">
        <div class="card-header bg-dark text-white text-center text-capitalize">
            <h2>Manage Super Administrators</h2>
        </div>
    </div>
    <div class="row mt-3 mb-3">
        <div class="col">
            <div class="input-group">
                <label class="input-group-text" for="search_box">Search by name or email:</label>
                <input type="text" class="form-control" name="search_box" id="search_box" value="{{ $viewData['search'] }}">
                <button class="btn btn-primary" id="search_button">Search</button>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($viewData['superAdmins'] as $superAdmin)
                    <tr>
                        <td>{{ $superAdmin->getId() }}</td>
                        <td>{{ $superAdmin->getName() }}</td>
                        <td>{{ $superAdmin->getEmail() }}</td>
                        <td>
                            <a class="btn btn-primary"
                                href="{{ route('superAdmin.edit', ['superAdmin' => $superAdmin->getId()]) }}">
                                <i class="bi-pencil"></i> Edit
                            </a>
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#deleteSuperAdminModal-{{ $superAdmin->getId() }}">
                                <i class="bi-trash"></i> Delete
                            </button>
                            <div class="modal fade" id="deleteSuperAdminModal-{{ $superAdmin->getId() }}" tabindex="-1"
                                aria-labelledby="deleteSuperAdminModal-{{ $superAdmin->getId() }}-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="deleteSuperAdminModal-{{ $superAdmin->getId() }}-label">
                                                Confirm Delete Admin</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete this admin?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Cancel</button>
                                            <form action="{{ route('superAdmin.destroy', $superAdmin->getId()) }}"
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
    
        </table>
        
    <div class="d-flex justify-content-center">
        {!! $viewData['superAdmins']->appends(request()->input()) !!}
    </div>
    </div>

    {{-- --------------------------------------------Create-super-admin------- --}}
    <div class="card">
        <div class="card-header">
            <h2>Create Admin</h2>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('superAdmin.store') }}">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name"
                            value="{{ old('name') }}" required autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required
                            autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label for="password-confirm"
                        class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label> --}}

                    {{-- <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required autocomplete="new-password">
                    </div> --}}
                {{-- </div> --}}

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Create Admin') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        document.getElementById('search_button').addEventListener('click', function() {
            let searchBoxValue = document.getElementById('search_box').value;
            window.location.href = '{{ route('superAdmin.index') }}?search=' + searchBoxValue;
        });
    </script>
@endsection
