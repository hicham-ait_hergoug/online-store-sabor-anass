@extends('layouts.app')
@section('title', $viewData['title'])
@section('subtitle', $viewData['subtitle'])
@section('content')

    <div class="row">
        <div class="col">
            <div class="mb-3 row-cols-1">
                <h6>{{__('messages.Filter by category')}}:</h6>
                <div class="mb-3 row-cols-1 ">
                    <select name="category" id="category_selected" aria-label="Filter by category">
                        <option value="-1">{{__('messages.all categories')}}</option>
                        @foreach ($viewData['categories'] as $item)
                            <option value="{{ $item->getId() }}"
                                {{ $item->getId() == $viewData['category'] ? 'selected' : '' }}>
                                {{ $item->getIntitule() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <div class="mb-3 row-cols-1">
                        <h6>{{__('messages.Filter by product in sale')}}</h6>
                        <select name="sale" id="sale_selected">
                            <option value="">{{__('messages.all products')}}</option>
<option value="yes" {{ isset($sale) && $sale == 'yes' ? 'selected' : '' }}>Discounted products</option>
                        </select>
                    </div>
                </div>

                @if (count($viewData['products']) == 0)
                    <h3>Aucun produit dans cette categorie</h3>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        @foreach ($viewData['products'] as $product)
            <div class="col-md-4 col-lg-3 mb-2">
                <div class="card">
                    <img src="{{ asset('/storage/' .$product->getImage()) }}" class="img-fluid rounded-start">
                    <div class="card-body text-center">
                        <a href="{{ route('product.show', ['id' => $product->getId()]) }}"
                            class="btn bg-primary text-white">{{ $product->getName() }}</a>
                    </div>
                    <div class="card">
                        <p>
                            <strong>Prix</strong>
                            @if ($product->hasActiveDiscount())
                                <del>{{ $product->getPrice() }}€</del>
                                <strong style="color:red">{{ $product->getSalePrice() }}€</strong>
                                @php
                                $discount = ($product->getPrice() - $product->getSalePrice()) / $product->getPrice() * 100;
                                @endphp
                            <span class="badge bg-danger">{{ round($discount) }}% off</span>
                             @else
                                <strong>{{ $product->getPrice() }}€</strong>
                            @endif
                        </p>
                    </div>

                    @if ($product->getQuantityStore() == 0)
                        <label class="btn -ml-1 btn-danger">Produit en rupture</label>
                    @endif
                </div>
            </div>
        @endforeach
        <div class="pagination justify-content-center">
          {{-- {{$viewData["products"]->links()}} --}}
          {{-- {{ $products->links() }} --}}

      </div>
      
      
        <script>
          // Filter products by category and sale status
          document.getElementById('category_selected').addEventListener('change', function(e) {
              const category = this.value;
              const sale = document.getElementById('sale_selected').value;
              let url = '/products';
              if (category !== '-1') {
                  url += '?category=' + category;
              }
              if (sale !== '') {
                  url += (url.indexOf('?') === -1 ? '?' : '&') + 'sale=' + sale;
              }
              window.location.href = url;
          });
      
          document.getElementById('sale_selected').addEventListener('change', function(e) {
              const sale = this.value;
              const category = document.getElementById('category_selected').value;
              let url = '/products';
              if (sale !== '') {
                  url += '?sale=' + sale;
              }
              if (category !== '-1') {
                  url += (url.indexOf('?') === -1 ? '?' : '&') + 'category=' + category;
              }
              window.location.href = url;
          });
      </script>
      

    </div>
@endsection
