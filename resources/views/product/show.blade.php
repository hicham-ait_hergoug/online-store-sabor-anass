@extends('layouts.app')

@section('title', $viewData['product']->getName())

@section('content')
    <div class="row">
        <div class="col-md-6">
            <img src="{{ asset('/storage/' . $viewData['product']->getImage()) }}" class="img-fluid rounded-start">
            {{-- <img src="{{ $viewData['product']->getImage() }}" class="img-fluid rounded-start"> --}}
        </div>
        <div class="col-md-6">
            <h1>{{ $viewData['product']->getName() }}</h1>
            <p>{{ $viewData['product']->getDescriptioN() }}</p>
            @if ($viewData['product']->hasActiveDiscount())
                <del>{{ $viewData['product']->getPrice() }}€</del>
                <strong style="color:red">{{ $viewData['product']->getSalePrice() }}€</strong>
                @php
                    $discount = (($viewData['product']->getPrice() - $viewData['product']->getSalePrice()) / $viewData['product']->getPrice()) * 100;
                @endphp
                <span class="badge bg-danger">{{ round($discount) }}% off</span>
            @else
                <strong>{{ $viewData['product']->getPrice() }}€</strong>
                {{-- @endif --}}
            @endif
            <p><strong>Availability:</strong> {!! $viewData['product']->getQuantityStore() > 0
                ? "<label class='label-stock bg-success'>In stock</label>"
                : "<label class='label-stock bg-danger'>Out of stock</label>" !!}</p>
            <form method="POST" action="{{ route('cart.add', ['id' => $viewData['product']->id]) }}">
                @csrf
                <div class="input-group mb-3">
                    <label class="input-group-text" for="quantity">Quantity:</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" min="1"
                        max="{{ $viewData['product']->getQuantityStore() }}" value="1">
                </div>
                <button type="submit" class="btn btn-primary"
                    {{ $viewData['product']->getQuantityStore() == 0 ? 'disabled' : '' }}>Add to cart</button>
            </form>
        </div>
    </div>
@endsection
