<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" />
    {{-- <?php
    $langue = app()->getLocale() == 'ar' ? '-rtl' : '';
    ?> --}}

    <title>@yield('title', 'Admin - Online Store')</title>
</head>

<body>
    <div class="row g-0">
        <div class="p-3 col fixed text-white bg-dark">
            <a href="{{ route('admin.home.index') }}" class="text-white text-decoration-none">
                <span class="mt-2 nav-link btn bg-info text-black">{{ __('messages.admin_panel') }}</span>
            </a>
            <hr />
            <ul class="nav flex-column">
                <li><a href="{{ route('admin.home.index') }}"
                        class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/home')) active @endif">-
                        Admin - {{ __('messages.home') }}</a></li>
                @if (auth()->user()->getRole() == 'super_admin')
                    <li><a href="{{ route('superAdmin.index') }}"
                            class="mt-2 nav-link btn bg-white text-black @if (Request::is('superAdmin/*')) active @endif">-
                            Admin - {{ __('messages.manage') }}</a></li>
                @endif
                <li><a href="{{ route('admin.product.index') }}"
                        class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/products*')) active @endif">-
                        Admin - {{ __('messages.products') }}</a></li>
                <li>
                    {{-- <ul> --}}
                <li><a
                        href="{{ route('admin.products.create') }}"class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/products/create')) active @endif">{{ __('messages.create') }}-
                        {{ __('messages.products') }}</a></li>
                {{-- <li><a href="#"class="nav-link text-white">Manage - Products</a></li>                --}}
                {{-- </ul> --}}
                </li>
                <li><a href="{{ route('admin.category.index') }}"
                        class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/categories*')) active @endif">-
                        Admin - {{ __('messages.categories') }}</a></li>

                <li><a href="{{ route('admin.suppliers.index') }}"
                        class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/suppliers*')) active @endif">-
                        Admin - {{ __('messages.suppliers') }}</a></li>
                <li><a href="{{ route('admin.discounts.index') }}"
                        class="mt-2 nav-link btn bg-white text-black @if (Request::is('admin/discounts*')) active @endif">-
                        Admin - {{ __('messages.discounts') }}</a></li>
                <li>
                    <a href="{{ route('home.index') }}" class="mt-2 btn bg-primary text-white">
                        {{ __('messages.go_back') }}</a>
                </li>
                <form id="logout" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                </form>
                <a href="#" class="mt-2 btn bg-danger text-white"
                    onclick="event.preventDefault(); document.getElementById('logout').submit();">
                    {{ __('messages.logout') }}
                </a>
            </ul>
        </div>

        <!-- sidebar -->
        <div class="col content-grey">
            <nav class="p-3 shadow d-flex justify-content-between align-items-center">
                <div>
                    <span class="profile-font">{{ auth()->user()->getName() }}</span>
                    <img class="img-profile rounded-circle" src="{{ asset('/img/undraw_profile.svg') }}">
                </div>
                <ul class="nav flex-column">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            {{ LaravelLocalization::getCurrentLocaleNative() }} <span
                                class="flag-icon flag-icon-{{ strtolower(LaravelLocalization::getCurrentLocale()) }}"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"
                                   href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    <span>
                                        @if ($properties['native'] == 'English')
                                            <i class="flag-icon flag-icon-gb"></i>
                                        @elseif($properties['native'] == 'العربية')
                                            <i class="flag-icon flag-icon-ma"></i>
                                        @elseif ($properties['native'] == 'Français')
                                            <i class="flag-icon flag-icon-fr"></i>
                                        @endif
                                    </span>
                                    {{ $properties['native'] }}
                                </a>
                            @endforeach
                        </div>
                        
                    </li>
                </ul>
            </nav>

            <div class="g-0 m-5">
                @yield('content')
            </div>
        </div>

        <!-- footer -->
        <div class="copyright py-4 text-center text-white">
            <div class="container">
                <small>
                    Copyright - <a class="text-reset fw-bold text-decoration-none" target="_blank"
                        href="https://twitter.com/danielgarax">
                        Daniel Correa
                    </a> - <b>Paola Vallejo</b>
                </small>
            </div>
        </div>
        <!-- footer -->

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
        </script>
</body>

</html>
