<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $response = $this->get(route('cart.index'));

        $response->assertStatus(200);
        $response->assertViewHas('viewData');
        $response->assertSee('Shopping Cart');
    }

    // public function testAdd()
    // {
    //     $product = Product::factory()->create();
    //     $quantity = 3;

    //     $response = $this->post(route('cart.add', ['id' => $product->getId()]), [
    //         'quantity' => $quantity,
    //     ]);

    //     $response->assertRedirect(route('cart.index'));

    //     $this->assertSessionHas('products');
    //     $productsInSession = session('products');
    //     $this->assertArrayHasKey($product->getId(), $productsInSession);
    //     $this->assertEquals($quantity, $productsInSession[$product->getId()]);
    // }

    // public function testDelete()
    // {
    //     $this->withSession(['products' => ['1' => 2, '2' => 1]]);

    //     $response = $this->post(route('cart.delete'));

    //     $response->assertRedirect();
    //     $this->assertSessionMissing('products');
    // }

    // public function testPurchase()
    // {
    //     $user = User::factory()->create(['balance' => 100]);
    //     $product1 = Product::factory()->create(['price' => 10]);
    //     $product2 = Product::factory()->create(['price' => 20]);
    //     $quantity1 = 3;
    //     $quantity2 = 1;

    //     $this->actingAs($user);

    //     $response = $this->post(route('cart.purchase'));

    //     $response->assertStatus(200);
    //     $response->assertViewHas('viewData');
    //     $response->assertSee('Purchase Status');

    //     $this->assertDatabaseHas('orders', [
    //         'user_id' => $user->getId(),
    //         'total' => $quantity1*$product1->getPrice() + $quantity2*$product2->getPrice(),
    //     ]);

    //     $order = Order::where('user_id', $user->getId())->firstOrFail();

    //     $this->assertDatabaseHas('items', [
    //         'product_id' => $product1->getId(),
    //         'order_id' => $order->getId(),
    //         'quantity' => $quantity1,
    //         'price' => $product1->getPrice(),
    //     ]);

    //     $this->assertDatabaseHas('items', [
    //         'product_id' => $product2->getId(),
    //         'order_id' => $order->getId(),
    //         'quantity' => $quantity2,
    //         'price' => $product2->getPrice(),
    //     ]);

    //     $this->assertDatabaseHas('users', [
    //         'id' => $user->getId(),
    //         'balance' => $user->getBalance() - ($quantity1*$product1->getPrice() + $quantity2*$product2->getPrice()),
    //     ]);

    //     $this->assertSessionMissing('products');
    // }
}
