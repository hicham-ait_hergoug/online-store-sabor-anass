<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Client\Request;
use App\Http\Controllers\ProductController;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }
    public function testProduct()
    {
        $response = $this->get(route('product.index'));

        $response->assertStatus(200);
        $response->assertViewHas('viewData');
        // $response->assertSee('Shopping Cart');
    }
    

//     public function testProductController()
// {
//     // Create a fake HTTP request with a "category" parameter
//     $request = Request::create('/products?category=1', 'GET');

//     // Instantiate the ProductController
//     $controller = new ProductController();

//     // Call the "index" method with the fake request
//     $response = $controller->index($request);

//     // Assert that the response is a view
//     $this->assertInstanceOf(View::class, $response);

//     // Assert that the view data contains the expected keys and values
//     $this->assertArrayHasKey('title', $response->getData()['viewData']);
//     $this->assertArrayHasKey('subtitle', $response->getData()['viewData']);
//     $this->assertArrayHasKey('categories', $response->getData()['viewData']);
//     $this->assertArrayHasKey('products', $response->getData()['viewData']);

//     $this->assertEquals('Products - Online Store', $response->getData()['viewData']['title']);
//     $this->assertEquals('List of products', $response->getData()['viewData']['subtitle']);
//     $this->assertCount(3, $response->getData()['viewData']['categories']);
//     $this->assertCount(2, $response->getData()['viewData']['products']);

//     // Call the "show" method with a product ID
//     $response = $controller->show(1);

//     // Assert that the response is a view
//     $this->assertInstanceOf(View::class, $response);

//     // Assert that the view data contains the expected keys and values
//     $this->assertArrayHasKey('title', $response->getData()['viewData']);
//     $this->assertArrayHasKey('subtitle', $response->getData()['viewData']);
//     $this->assertArrayHasKey('product', $response->getData()['viewData']);

//     $this->assertEquals('Product 1 - Online Store', $response->getData()['viewData']['title']);
//     $this->assertEquals('Product 1 - Product information', $response->getData()['viewData']['subtitle']);
//     $this->assertInstanceOf(Product::class, $response->getData()['viewData']['product']);
// }

}
