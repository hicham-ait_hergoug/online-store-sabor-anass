<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     *
     */
    
    

    public function run()
    {
        // Generate categories and suppliers
        $categories = Category::factory()->count(5)->create();
        $suppliers = Supplier::factory()->count(10)->create();
     
        // Generate products with random category and supplier assignments
        $products = Product::factory()->count(20)
            ->state(function (array $attributes) use ($categories, $suppliers) {
                return [
                    'category_id' => $categories->random()->id,
                    'supplier_id' => $suppliers->random()->id,
                ];
            })
            ->create();
        $user=User::factory()->count(10)->create();

        //  Generate an order with 3 random products
        // $order = Order::factory()
        // ->create([
        //     'user_id' => $user->random()->id,
        // ]);
        // ->each(function ($order) {
        //     $products = Product::all();
        //     $order->items()->saveMany(
               // Item::factory()->count(3)->create([
               //     'product_id' => $products->random()->id,
               //     'order_id'=>3
               // ]);
        //     );
        // });
    }
}
