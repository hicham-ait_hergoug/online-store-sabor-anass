<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // $response = Http::get('https://api.github.com/users');
        // $users = $response->json();
    
        // $imageUrls = [];
        // foreach ($users as $user) {
        //     $imageUrl = $user.avatar_url;
        //     $imageUrls[] = $imageUrl;
        // }
    
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(2),
            'image' => $this->faker->imageUrl(640, 480, 'animals', true),
            'price' => $this->faker->numberBetween(10, 100),
            'quantity_store' => $this->faker->numberBetween(10, 100),
            // 'category_id' => Category::factory(),
            // 'supplier_id' => Supplier::factory()
        ];
    }
}
