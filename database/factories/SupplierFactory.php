<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Supplier>
 */
class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'full_name'=>$this->faker->firstName . ' ' . $this->faker->lastName,
            'raison_sociale'=>$this->faker->company(),
            'city'=>$this->faker->city(),
            'adresse'=>$this->faker->address(),
            'telephone'=>$this->faker->phoneNumber(),
            'email'=>$this->faker->email(),
            'description' => $this->faker->sentence(2),
            


        ];
    }
    public function withRandomCategory()
    {
        return $this->state(function (array $attributes) {
            $category = Category::inRandomOrder()->first();
            return [
            'category_id' => $category->id,
            ];
        });
    }
}
