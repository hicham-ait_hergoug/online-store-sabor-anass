<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    protected $fillable = [
        'full_name','raison_sociale', 'adresse', 'telephone', 'email', 'description','city'
    ];
    public static function validate($request)
    {
        $request->validate([
            'full_name' => 'required|max:255',
            'raison_sociale' => 'required|max:255',
            'adresse' => 'required|max:255',
            'city' => 'required|max:255',
            'telephone' => 'required|max:20',
            'email' => 'required|email|max:255',
            'description' => 'nullable',
        ]);
    }
    public function getId()
    {
        return $this->attributes["id"];
    }
    public function setId($id)
    {
        $this->attributes["id"] = $id;
    }
    public function getCategoryId()
    {
        return $this->attributes['category_id'];
    }
    public function setCategoryId($id)
    {
        $this->attributes['category_id'] = $id;
    }
    public function getFullName()
    {
        return $this->attributes["full_name"];
    }
    public function setFullName($full_name)
    {
        $this->attributes["full_name"] = $full_name;
    }
    public function getRaisonSociale()
    {
        return $this->attributes["raison_sociale"];
    }
    public function setRaisonSociale($RaisonSociale)
    {
        $this->attributes["raison_sociale"] = $RaisonSociale;
    }public function getAdresse()
    {
        return $this->attributes["adresse"];
    }
    public function setAdresse($adresse)
    {
        $this->attributes["adresse"] = $adresse;
    }public function getCity()
    {
        return $this->attributes["city"];
    }
    public function setCity($city)
    {
        $this->attributes["city"] = $city;
    }
    public function getTelephone()
    {
        return $this->attributes["telephone"];
    }
    public function setTelephone($telephone)
    {
        $this->attributes["telephone"] = $telephone;
    }
    
    public function getEmail()
    {
        return $this->attributes["email"];
    }
    public function setEmail($email)
    {
        $this->attributes["email"] = $email;
    }
    
    public function getDescription()
    {
        return $this->attributes["description"];
    }
    public function setDescription($description)
    {
        $this->attributes["description"] = $description;
    }
    public function products()
    {
        // return $this->hasManyThrough(Product::class, SupplierProduct::class, 'supplier_id', 'id', 'id', 'product_id');
        return $this->hasMany(Supplier::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }
}
