<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    /**
     * PRODUCT ATTRIBUTES
     * $this->attributes['id'] - int - contains the product primary key (id)
     * $this->attributes['name'] - string - contains the product name
     * $this->attributes['description'] - string - contains the product description
     * $this->attributes['image'] - string - contains the product image
     * $this->attributes['price'] - int - contains the product price
     * $this->attributes['created_at'] - timestamp - contains the product creation date
     * $this->attributes['updated_at'] - timestamp - contains the product update date
     * $this->items - Item[] - contains the associated items
     */

    public static function validate($request)
    {
        $request->validate([
            "name" => "required|max:255",
            "description" => "required",
            "price" => "required|numeric|gt:0",
            'image' => 'image',
            'quantity_store'=>"required|numeric",
            'category'=>"required"
        ]);
    }

    public static function sumPricesByQuantities($products, $productsInSession)
    {
        $total = 0;
        foreach ($products as $product) {
            if ($product->hasActiveDiscount()) {
                $total += $product->getSalePrice() * $productsInSession[$product->getId()];
            } else {
                $total += $product->getPrice() * $productsInSession[$product->getId()];
            }
        }
        return $total;
    }

    public function getId()
    {
        return $this->attributes['id'];
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function getCategoryId()
    {
        return $this->attributes['category_id'];
    }
    public function setCategoryId($id)
    {
        $this->attributes['category_id'] = $id;
    }
    public function getSupplierId()
    {
        return $this->attributes['supplier_id'];
    }
    public function setSupplierId($id)
    {
        $this->attributes['supplier_id'] = $id;
    }

    public function setId($id)
    {
        $this->attributes['id'] = $id;
    }

    public function getName()
    {
        return $this->attributes['name'];
    }

    public function setName($name)
    {
        $this->attributes['name'] = $name;
    }

    public function getDescription()
    {
        return $this->attributes['description'];
    }

    public function setDescription($description)
    {
        $this->attributes['description'] = $description;
    }

    public function getImage()
    {
        return $this->attributes['image'];
    }

    public function setImage($image)
    {
        $this->attributes['image'] = $image;
    }

    public function getPrice()
    {
        return $this->attributes['price'];
    }
    public function getOldPrice()
    {
        return $this->old_price;
    }
    public function setPrice($price)
    {
        $this->attributes['price'] = $price;
    }

    public function getCreatedAt()
    {
        return $this->attributes['created_at'];
    }

    public function setCreatedAt($createdAt)
    {
        $this->attributes['created_at'] = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->attributes['updated_at'];
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->attributes['updated_at'] = $updatedAt;
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }
    
    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items)
    {
        $this->items = $items;
    }
   public function getQuantityStore(){
    return  $this->attributes["quantity_store"];
   }
   public function setQuantityStore($value){
    $this->attributes["quantity_store"]=$value;
   }
   public static function setDiscountForAllProducts($discount)
{
    Product::query()->update(['discount' => $discount]);
}
public static function setDiscountForCategory($category_id, $discount)
{
    Product::where('category_id', $category_id)->update(['discount' => $discount]);
}
public static function setDiscountForProduct($product_id, $discount)
{
    Product::where('id', $product_id)->update(['discount' => $discount]);
}
public function getSalePrice()
{
    if ($this->discount) {
        return round($this->price * (1 - $this->discount / 100), 2);
    } else {
        return $this->price;
    }
}
public function discounts()
{
    return $this->belongsToMany(Discount::class);
}
public static function discountedProducts()
{
    return self::where('discount', '>', 0)->get();
}
public function hasActiveDiscount()
{
    $now = Carbon::now();

    // Check if the product has an active discount
    $hasProductDiscount = $this->discounts()
        ->where('start_date', '<=', $now)
        ->where('end_date', '>=', $now)
        ->exists();

    if ($hasProductDiscount) {
        return true;
    }

    $hasCategoryDiscount = $this->category() 
        ->whereHas('discounts', function ($query) use ($now) {
            $query->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now);
        })
        ->exists();

    if ($hasCategoryDiscount) {
        return true;
    }

    $globalDiscount = Discount::where('apply_to', 'all')
        ->where('start_date', '<=', $now)
        ->where('end_date', '>=', $now)
        ->first();

    
    if ($globalDiscount) {
        if ($globalDiscount->apply_to === 'all') {
            return true;
        } else {
            $productInDiscount = $this->discounts()
                ->where('discounts.id', $globalDiscount->id)
                ->exists();
            if ($productInDiscount) {
                return true;
            }
        }
    }

    return false;
}


public function supplier() {
     return $this->belongsTo(Supplier::class);
     }
}

