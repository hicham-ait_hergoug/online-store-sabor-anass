<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public static function validate($request)
    {
        $request->validate([
            "intitule" => "required|max:255",
            "description" => "required"
        ]);
    }    public function getId()
    {
        return $this->attributes["id"];
    }
    public function setId($id)
    {
        $this->attributes["id"] = $id;
    }
    public function getSupplierId()
    {
        return $this->attributes['supplier_id'];
    }
    public function setSupplierId($id)
    {
        $this->attributes['supplier_id'] = $id;
    }
    public function getIntitule()
    {
        return $this->attributes["intitule"];
    }
    public function setIntitule($intitule)
    {
        $this->attributes["intitule"] = $intitule;
    }
    public function getDescription()
    {
        return $this->attributes["description"];
    }
    public function setDescription($description)
    {
        $this->attributes["description"] = $description;
    }
    public function Products()
    {
        return $this->hasMany(Category::class);
    }
    public function supplier()
    {
        return $this->belongsToMany(Supplier::class);
    }
    public function discounts()
    {
        return $this->belongsToMany(Discount::class);
    }
    public function getIntituleDiscount()
    {
        return $this->hasMany('App\Models\Discount', 'foreign_key');
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
