<?php
namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Order;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    // en utlisant variable les cookies
    public function index(Request $request)
    {
        $total = 0;
        $productsInCart = [];

        $productsInCookie =json_decode($request->cookie("products"), true);

        if ($productsInCookie) {
            $productsInCart = Product::findMany(array_keys($productsInCookie));
            $total = Product::sumPricesByQuantities($productsInCart, $productsInCookie);
        }

        $viewData = [];
        $viewData["title"] = "Cart - Online Store";
        $viewData["subtitle"] =  "Shopping Cart";
        $viewData["total"] = $total;
        $viewData["products"] = $productsInCart;
        return view('cart.index', compact('viewData', 'productsInCookie'));
    }

    public function add(Request $request, $id)
    {
        $quantity = $request->input('quantity') ?? 1;
        $productsInCookie = json_decode($request->cookie("products"), true);
        $productsInCookie[$id] = $quantity;
        $cookie = cookie('products', json_encode($productsInCookie), 60*24*7); // cookie expires in 7 days
        return redirect()->route('cart.index')->cookie($cookie);
    }
    

    public function delete(Request $request)
    {
        $productId = $request->input('product_id');
        $productsInCookie = json_decode($request->cookie("products"), true);
    
        if ($productId) {
            unset($productsInCookie[$productId]);
        } else {
            $productsInCookie = [];
        }
    
        return redirect()->route('cart.index')->cookie('products', json_encode($productsInCookie));
    }
    
    public function purchase(Request $request)
    {
        $productsInCookie = json_decode($request->cookie("products"), true);
        if ($productsInCookie) {
            $userId = Auth::user()->getId();
            $order = new Order();
            $order->setUserId($userId);
            $order->setTotal(0);
            $order->save();
    
            $total = 0;
            $productIds = array_keys($productsInCookie);
            $productsInCart = Product::whereIn('id', $productIds)->get();
            foreach ($productsInCart as $product) {
                $quantity = $productsInCookie[$product->getId()];
                $item = new Item();
                $item->setQuantity($quantity);
                $item->setPrice($product->getSalePrice() ? $product->getSalePrice() : $product->getPrice());
                $item->setProductId($product->getId());
                $item->setOrderId($order->getId());
                $item->save();
                
                if ($product->getQuantityStore() < $quantity) {
                    return redirect()->back()->with('error', 'Insufficient quantity in stock.');
                }
                $product->setQuantityStore($product->getQuantityStore() - $quantity);
                $product->save();
                $total = $total + (($product->getSalePrice() ?? $product->getPrice())*$quantity);
            }
            $order->setTotal($total);
            $order->save();
    
            $newBalance = Auth::user()->getBalance() - $total;
            Auth::user()->setBalance($newBalance);
            $request->session()->forget('products');
            $viewData = [];
            $viewData["title"] = "Purchase - Online Store";
            $viewData["subtitle"] =  "Purchase Status";
            $viewData["order"] =  $order;
            return view('cart.purchase')->with("viewData", $viewData);
            // return redirect()->route('cart.purchase')->with('success', 'Purchase successful.');
        } else {
            return redirect()->route('cart.index')->with('error', 'No products in cart.');
        }
    }
}
    
    // en utlisant variable de session
    // public function index(Request $request)
    // {
    //     $total = 0;
    //     $productsInCart = [];

    //     $productsInSession = $request->session()->get("products");
    //     if ($productsInSession) {
    //         $productsInCart = Product::findMany(array_keys($productsInSession));
    //         $total = Product::sumPricesByQuantities($productsInCart, $productsInSession);
    //     }

    //     $viewData = [];
    //     $viewData["title"] = "Cart - Online Store";
    //     $viewData["subtitle"] =  "Shopping Cart";
    //     $viewData["total"] = $total;
    //     $viewData["products"] = $productsInCart;
    //     return view('cart.index')->with("viewData", $viewData);
    // }

    // public function add(Request $request, $id)
    // {
    //     $products = $request->session()->get("products");
    //     $products[$id] = $request->input('quantity');
    //     $request->session()->put('products', $products);

    //     return redirect()->route('cart.index');
    // }

    // public function delete(Request $request)
    // {
    //     $request->session()->forget('products');
    //     return back();
    // }

    // public function purchase(Request $request)
    // {
    //     $productsInSession = $request->session()->get("products");
    //     if ($productsInSession) {
    //         $userId = Auth::user()->getId();
    //         $order = new Order();
    //         $order->setUserId($userId);
    //         $order->setTotal(0);
    //         $order->save();

    //         $total = 0;
    //         $productsInCart = Product::findMany(array_keys($productsInSession));
    //         foreach ($productsInCart as $product) {
    //             $quantity = $productsInSession[$product->getId()];
    //             $item = new Item();
    //             $item->setQuantity($quantity);
    //             $item->setPrice($product->getSalePrice() ? $product->getSalePrice() : $product->getPrice()); // modify this line
    //             $item->setProductId($product->getId());
    //             $item->setOrderId($order->getId());
    //             $item->save();
    //             if (!$product) {
    //                 return redirect()->back()->with('error', 'Product not found.');
    //             }
            
    //             if ($product->getQuantityStore() < $quantity) {
    //                 return redirect()->back()->with('error', 'Insufficient quantity in stock.');
    //             }
    //             $product->setQuantityStore($product->getQuantityStore() - $quantity);
    //             $product->save();
    //             $total = $total + (($product->getSalePrice() ?? $product->getPrice())*$quantity);
    //         }
    //         $order->setTotal($total);
    //         $order->save();

    //         $newBalance = Auth::user()->getBalance() - $total;
    //         Auth::user()->setBalance($newBalance);
    //         Auth::user()->save();

    //         $request->session()->forget('products');

    //         $viewData = [];
    //         $viewData["title"] = "Purchase - Online Store";
    //         $viewData["subtitle"] =  "Purchase Status";
    //         $viewData["order"] =  $order;
    //         return view('cart.purchase')->with("viewData", $viewData);
    //     } else {
    //         return redirect()->route('cart.index');
    //     }
    // }
