<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminStatsController extends Controller
{
    public function index(Request $request)
    {
        $stats = [
            'ca' => $this->getSalesStats($request),
            'profit' => $this->getProfitStats($request),
        ];

        return view('stats.index', compact('stats'));
    }

    private function getSalesStats(Request $request)
    {
        $groupBy = $request->input('group_by', 'day');
        $start = $request->input('start', date('Y-m-d', strtotime('-1 month')));
        $end = $request->input('end', date('Y-m-d'));

        $salesStats = DB::table('orders')
            ->select(DB::raw("SUM(total_amount) as total, DATE_FORMAT(created_at, '%Y-%m-%d') as date, products.name as product_name, categories.intitule as category_name, orders.country as country"))
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->whereBetween('created_at', [$start, $end])
            ->groupBy('date');

        switch ($groupBy) {
            case 'month':
                $salesStats->select(DB::raw("SUM(total_amount) as total, DATE_FORMAT(created_at, '%Y-%m') as date, products.name as product_name, categories.intitule as category_name, orders.country as country"))
                    ->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y-%m')"));
                break;
            case 'year':
                $salesStats->select(DB::raw("SUM(total_amount) as total, DATE_FORMAT(created_at, '%Y') as date, products.name as product_name, categories.intitule as category_name, orders.country as country"))
                    ->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y')"));
                break;
            case 'product':
                $salesStats->select(DB::raw("SUM(total_amount) as total, products.name as product_name, categories.intitule as category_name, orders.country as country"))
                    ->groupBy('products.id');
                break;
            case 'category':
                $salesStats->select(DB::raw("SUM(total_amount) as total, categories.intitule as category_name, orders.country as country"))
                    ->groupBy('categories.id');
                break;
            case 'country':
                $salesStats->select(DB::raw("SUM(total_amount) as total, orders.country as country"))
                    ->groupBy('orders.country');
                break;
        }

        return $salesStats->get();
    }

    private function getProfitStats(Request $request)
    {
        // Same logic as getSalesStats but with profit calculation
    }
}
