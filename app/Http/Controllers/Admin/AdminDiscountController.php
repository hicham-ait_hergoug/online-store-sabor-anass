<?php


namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Category;
use App\Models\Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDiscountController extends Controller
{
    public function create()
    {
        $viewData = [];
        $viewData["title"] = 'Create Discount';
        $viewData["categories"] = Category::all();
        $viewData["products"] = Product::all();


        return view("admin.discounts.create")->with("viewData", $viewData);
    }

    public function store(Request $request)
    {
        $viewData = [];
        $viewData["title"] = 'Discounts';
        $request->validate([
            'name' => 'required',
            'description' => 'nullable',
            'value' => 'required|numeric|min:1|max:80',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'apply_to' => 'required|in:all,categories,products',
            'categories' => 'nullable|array',
            'products' => 'nullable|array',
        ]);

        $discount = Discount::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'value' => $request->input('value'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'apply_to' => $request->input('apply_to'),
            'category_id' => null,
            'product_id' => null


        ]);


        if ($request->input('apply_to') === 'categories') {
            // $discount->categories()->sync($viewData["categories"]);
            $viewData["categories"] = Category::whereIn('id', $request->input('categories'))->get();
            $discount->categories()->sync($viewData["categories"]);
            foreach ($viewData["categories"] as $category) {
                Product::setDiscountForCategory($category->id, $request->input('value'));
            }
            $discount->category_id = $request->input('categories')[0]; // assuming only one category is selected
        } elseif ($request->input('apply_to') === 'products') {
            $viewData["products"] = Product::whereIn('id', $request->input('products'))->get();
            $discount->products()->sync($viewData["products"]);
            foreach ($viewData["products"] as $product) {
                Product::setDiscountForProduct($product->id, $request->input('value'));
            }
            $discount->product_id = $request->input('products')[0]; // assuming only one product is selected
        } else {
            $request->input('apply_to') === 'all';
            $discount->for_all_products = true;
            Product::setDiscountForAllProducts($request->input('value'));
        }
        $discount->save();
            // dd($request->all());

        return redirect()->route('admin.discounts.index')->with([
            'viewData' => $viewData,
            'success' => 'Discount created successfully'
        ]);
    }

    // public function destroy(Request $request, Discount $discount)
    // {
    //     $discount->delete();
    //     return redirect()->route('admin.discounts.index')
    //         ->with('success', 'Discount deleted successfully');

    // }

    public function index()
    {
        $viewData = [];
        $viewData["title"] = 'Discounts';
        $viewData["discounts"] = Discount::with(['categories', 'products'])->paginate(10);
        return view("admin.discounts.index")->with("viewData", $viewData);
    }
    public function edit(Discount $discount)
    {
        $viewData = [];
        $viewData["title"] = 'Edit Discount';
        $viewData["categories"] = Category::all();
        $viewData["products"] = Product::all();

        return view("admin.discounts.edit", compact('viewData', 'discount'));
    }
    public function update(Request $request, Discount $discount)
    {
        $request->validate([
        'name' => 'required',
        'description' => 'nullable',
        'value' => 'required|numeric|min:1|max:80',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after:start_date',
        'apply_to' => 'required|in:all,categories,products',
        'categories' => 'nullable|array',
        'products' => 'nullable|array',
        ]);

    // Update the discount's attributes
        $discount->update([
        'name' => $request->input('name'),
        'description' => $request->input('description'),
        'value' => $request->input('value'),
        'start_date' => $request->input('start_date'),
        'end_date' => $request->input('end_date'),
        'apply_to' => $request->input('apply_to'),
        'category_id' => null,
        'product_id' => null,
        ]);

    // Update the discount's relationships based on the "apply_to" field
        if ($request->input('apply_to') === 'categories') {
            $categories = $request->input('categories');
            if (!empty($categories)) {
                // Update the discount's categories
                $viewData["categories"] = Category::whereIn('id', $categories)->get();
                $discount->categories()->sync($categories);

                // Update the discount's discount value for each product in the selected categories
                foreach ($viewData["categories"] as $category) {
                    Product::setDiscountForCategory($category->id, $request->input('value'));
                }

                // Set the category ID for the discount (assuming only one category is selected)
                $discount->category_id = $categories[0];
                $discount->product_id = null;
            } else {
                // If no categories are selected, remove the discount's categories
                $discount->categories()->detach();
            }
        } elseif ($request->input('apply_to') === 'products') {
            $products = $request->input('products');
            if (!empty($products)) {
                // Update the discount's products
                $viewData["products"] = Product::whereIn('id', $products)->get();
                $discount->products()->sync($products);

                // Update the discount's discount value for each selected product
                foreach ($viewData["products"] as $product) {
                    Product::setDiscountForProduct($product->id, $request->input('value'));
                }

                // Set the product ID for the discount (assuming only one product is selected)
                $discount->product_id = $products[0];
                $discount->category_id = null;
            } else {
                // If no products are selected, remove the discount's products
                $discount->products()->detach();
            }
        } else {
            // If "apply_to" is set to "all", set the discount value for all products
            $discount->for_all_products = true;
            Product::setDiscountForAllProducts($request->input('value'));

            // Remove the discount's categories and products
            $discount->categories()->detach();
            $discount->products()->detach();

            // Set the category and product IDs to null
            $discount->category_id = null;
            $discount->product_id = null;
        }

    // Save the updated discount

            $discount->save();

            // dd($request->all(), $discount->toArray());

            return redirect()->route('admin.discounts.index')
                ->with('success', 'Discount updated successfully');
    }
    public function destroy(Discount $discount)
    {
        // Check if the discount has expired
        if ($discount->end_date < now()) {
            // Discount has expired, remove the discount value from the products table for the affected categories and products
            if ($discount->apply_to === 'categories') {
                $categories = $discount->categories;
                foreach ($categories as $category) {
                    Product::setDiscountForCategory($category->id, 0);
                    Product::removeDiscountForCategory($category->id);
                }
            } elseif ($discount->apply_to === 'products') {
                $products = $discount->products;
                foreach ($products as $product) {
                    Product::setDiscountForProduct($product->id, 0);
                }
            } else {
                Product::setDiscountForAllProducts(0);
            }
        
            // Delete the discount
            $discount->delete();
                
            return redirect()->route('admin.discounts.index')
                ->with('success', 'Discount deleted successfully');
        } else {
            // Discount has not expired, update the discount value in the products table for the affected categories and products
            if ($discount->apply_to === 'categories') {
                $categories = $discount->categories;
                foreach ($categories as $category) {
                    Product::setDiscountForCategory($category->id, 0);
                }
            } elseif ($discount->apply_to === 'products') {
                $products = $discount->products;
                foreach ($products as $product) {
                    Product::setDiscountForProduct($product->id, 0);
                }
            } else {
                Product::setDiscountForAllProducts(0);
            }
        
            // Delete the discount
            $discount->delete();
        
            return redirect()->route('admin.discounts.index')
                ->with('success', 'Discount deleted successfully');
        }
    }
}
