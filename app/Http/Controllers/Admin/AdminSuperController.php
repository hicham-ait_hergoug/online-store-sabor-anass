<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

/**
 * Summary of AdminSuperController
 */
class AdminSuperController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $query = User::query();

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%");
            });
        }
        $query->where('role', '=', 'admin');
        $superAdmins = $query->paginate(10);

        $viewData = [
            'title' => 'Manage Super Administrators',
            'search' => $search,
            'superAdmins' => $superAdmins,
        ];

        return view('admin.superAdmin.index', compact('viewData'));
    }
    /**
     * Summary of show
     * @param User $admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request)
    {
        $admin=User::all();
        $search = $request->input('search');
        $query = User::query();

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%");
            });
        }

        $viewData = [
            'title' => 'Manage Super Administrators',
            'search' => $search,
            'superAdmins' => $admin,
        ];
        return view('admin.superAdmin.index', compact('viewData'));
    }
    public function create(Request $request)
    {
        // Vérifier que l'utilisateur actuel a un rôle de "superadmin"
        if (auth()->user()->role !== 'superadmin') {
            abort(403, 'Unauthorized action.');
        }

        // Créer un nouvel utilisateur avec un rôle de "admin"
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = 'admin';
        // $user->balance = 5000;

        $user->save();

        return redirect()->route('superAdmin.index')->with('success', 'Admin created successfully!');
    }
    public function store(Request $request)
    {
        $admin = new User;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->role = 'admin';
        $admin->save();

        return redirect()->route('superAdmin.index')->with('success', 'Admin created successfully!');
    }

    public function edit(Request $resquest, $id)
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Edit Product - Online Store";
        $viewData["user"] = User::findOrFail($id);
        return view('admin.superAdmin.edit', compact('id', 'viewData'));
    }

   
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->save();
    
        return redirect()->route('superAdmin.index', compact('id'))->with('success', 'Admin updated successfully!');
    }
    

    public function destroy($id)
    {
        $superAdmin = User::findOrFail($id);

        if (!$superAdmin) {
            return redirect()->route('superAdmin.index')->with('error', 'Admin not found!');
        }
   
        $superAdmin->delete();
   
        return redirect()->route('superAdmin.index')->with('success', 'Super Admin deleted successfully!');
    }
}
