<?php

namespace App\Http\Controllers\Admin;

use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSupplierController extends Controller
{
    public function index()
    {
        $viewData = [];
        $viewData["title"] = 'Suppliers';
        $viewData["suppliers"] = Supplier::paginate(10);
        return view("admin.suppliers.index")->with("viewData", $viewData);
    }
    public function create()
    {
        $viewData = [];
        $viewData["title"] = "Create supplier - Online Store";
        $viewData["subtitle"] =  "Create a new supplier";
        return view('admin.suppliers.create')->with("viewData", $viewData);
    }
    
    public function store(Request $request)
    {
        Supplier::validate($request);
        $supplier = new Supplier();
        $supplier->fill($request->all());
        $supplier->save();
        return redirect()->route('admin.suppliers.index');
    }
    
    public function edit(Request $request, $id)
    {
       
            $viewData = [];
            $supplier = Supplier::findOrFail($id);
            $viewData["title"] = "Edit " . $supplier->getFullName() . " - Online Store";
            $viewData["subtitle"] =  "Edit supplier information";
            $viewData["supplier"] = $supplier;
            return view('admin.suppliers.edit')->with("viewData", $viewData);
    }
    
    // public function update(Request $request, Supplier $supplier)
    // {
    //     $supplier = Supplier::findOrFail($supplier);
    //     Supplier::validate($request);
    //     $supplier->fill($request->all());
    //     $supplier->save();
    //     return redirect()->route('admin.suppliers.show', $supplier);
    // }
    public function update(Request $request, Supplier $supplier)
    {
        $request->validate([
        'full_name' => 'required|string|max:255',
        'raison_sociale' => 'required|string|max:255',
        'adresse' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'telephone' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'description' => 'nullable|string',
        ]);

        $supplier->full_name = $request->input('full_name');
        $supplier->raison_sociale = $request->input('raison_sociale');
        $supplier->adresse = $request->input('adresse');
        $supplier->adresse = $request->input('city');
        $supplier->telephone = $request->input('telephone');
        $supplier->email = $request->input('email');
        $supplier->description = $request->input('description');

        $supplier->save();

        return redirect()->route('admin.suppliers.index')->with('success', 'Supplier updated successfully!');
    }

    
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        return redirect()->route('admin.suppliers.index')->with('success', 'Supplier has been deleted!');
    }
}
