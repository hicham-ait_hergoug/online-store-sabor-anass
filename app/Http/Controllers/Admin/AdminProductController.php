<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AdminProductController extends Controller
{
    public function index(Request $request)
    {
        $viewData = [];
        
        $viewData["title"] = "Admin Page - Products - Online Store";
        $viewData["category"] = $request->query('category');
        $viewData["subtitle"] =  "List of products";
        $viewData["categories"]=Category::all();
        $viewData["products"] = Product::all();
        // $viewData["products"] = Product::whereHas('discounts')->get();
        $viewData["discounts"] = Discount::with(['categories', 'products'])->get();
        $viewData["suppliers"]=Supplier::all();
        $viewData["supplier"] = $request->query('supplier');


    //    $categoryId = $request->input('category');
        if ($viewData["category"] && $viewData["category"]!=-1) {
            $viewData["products"]=Product::where('category_id', $viewData["category"])->get();
        }
        if ($viewData["supplier"] && $viewData["supplier"] != -1) {
            $viewData["products"] = Product::where('supplier_id', $viewData["supplier"])->get();
        }

        return view('admin.product.index')->with("viewData", $viewData);
    }

    public function store(Request $request)
    {
        Product::validate($request);

        $newProduct = new Product();
        $newProduct->setName($request->input('name'));
        $newProduct->setDescription($request->input('description'));
        $newProduct->setPrice($request->input('price'));
        $newProduct->setQuantityStore($request->input('quantity_store'));
        $newProduct->setCategoryId($request->get('category'));
        $newProduct->setSupplierId($request->input('supplier'));

        $newProduct->setImage("game.png");
        $newProduct->save();

        if ($request->hasFile('image')) {
            $imageName = $newProduct->getId().".".$request->file('image')->extension();
            Storage::disk('public')->put(
                $imageName,
                file_get_contents($request->file('image')->getRealPath())
            );
            $newProduct->setImage($imageName);
            $newProduct->save();
        }

        return back();
    }

    public function delete($id)
    {
        Product::destroy($id);
        return back();
    }

    public function edit($id)
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Edit Product - Online Store";
        $viewData["product"] = Product::findOrFail($id);
        $viewData["categories"] = Category::all();
        $viewData["suppliers"]=Supplier::all();

        return view('admin.product.edit')->with("viewData", $viewData);
    }

    public function update(Request $request, $id)
    {
        Product::validate($request);

        $product = Product::findOrFail($id);
        $product->setName($request->input('name'));
        $product->setDescription($request->input('description'));
        $product->setPrice($request->input('price'));
        $product->setQuantityStore($request->input('quantity_store'));
        $product->setCategoryId($request->get('category'));
        $product->setSupplierId($request->input('supplier'));


        

        if ($request->hasFile('image')) {
            $imageName = $product->getId().".".$request->file('image')->extension();
            Storage::disk('public')->put(
                $imageName,
                file_get_contents($request->file('image')->getRealPath())
            );
            $product->setImage($imageName);
        }

        $product->save();
        return redirect()->route('admin.product.index');
    }
    public function create()
    {
        $viewData=[];
        $viewData["subtitle"] =  "Products";
        $viewData["categories"]=Category::all();
        $viewData["products"] = Product::all();
        $viewData["suppliers"]=Supplier::all();
        $viewData["title"]='create peroduct';
        return view("admin.product.create")->with("viewData", $viewData);
    }
}
