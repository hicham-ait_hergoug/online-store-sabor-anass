<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\Console\Input\Input;

// use illuminate\support\Facades\Input;
class ProductController extends Controller
{
    public function index(Request $request)
    {
        $viewData = [];
        $viewData["title"] = "Products - Online Store";
        $viewData["category"] = $request->query('category');
        $viewData["subtitle"] =  "List of products";
        $viewData["categories"] = Category::all();
        $viewData["products"] = Product::all();

       
        $sale = $request->query('sale', '');

        if ($sale == 'yes') {
            $productSale = Product::discountedProducts();

            if ($viewData["category"] && $viewData["category"] != -1) {
                $productSale = $productSale->where('category_id', $viewData["category"]);
            }

            $viewData["products"] = $productSale->filter(function ($product) {
                return $product->hasActiveDiscount();
            });
        } elseif ($sale == 'no') {
            $products = Product::query();

            if ($viewData["category"] && $viewData["category"] != -1) {
                $products = $products->where('category_id', $viewData["category"]);
            }

            $viewData["products"] = $products->filter(function ($product) {
                return $product->hasActiveDiscount();
            });
        } elseif ($viewData["category"] && $viewData["category"] != -1) {
            $viewData["products"] = Product::where('category_id', $viewData["category"])->get();
        } elseif ($sale == '') {
            $viewData["products"] = Product::all();
        }

        return view('product.index')->with("viewData", $viewData);
    }


    public function show($id)
    {
        $viewData = [];
        $product = Product::findOrFail($id);
        $viewData["title"] = $product->getName() . " - Online Store";
        $viewData["subtitle"] =  $product->getName() . " - Product information";
        $viewData["product"] = $product;
        return view('product.show')->with("viewData", $viewData);
    }
}
